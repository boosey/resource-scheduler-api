var express = require('express');
var bodyParser = require('body-parser')
var router = express.Router()
var fs = require('fs')
var path = require('path')
const commands = require('../CQRS/resource-commands.js')
const queries = require('../CQRS/resource-queries.js')

router.use('/', bodyParser.json());
router.use('/', bodyParser.urlencoded({ extended: true }));

router.use(['/', '/:resourceId'], function (req, res, next) {
  if (req.method === 'POST' || req.method === 'PUT' ||
    req.method === 'DELETE' ) {
    if (commands.ready()) {
      next()
    } else {
      console.log('producer not available')
      res.status(500).send('Cannot save resource - command module not ready')
    }
  } else {
    next()
    // if (queries.ready()) {
    //   next()
    // } else {
    //   console.log('producer not available')
    //   res.status(500).send('Cannot save resource - command module not ready')
    // }
  }
})

const handleRoute = async function(req, res, next, handler, errorHandler) {
  try {
    return await handler(req, res, next)
  } catch(err) {
    if (errorHandler) {
      errorHandler(err)
    } else {
      res.status(500).send(err.message)
    }
  }
}

router.get('/', async function(req, res, next) {
  return await handleRoute(req, res, next,
    async function(req, res, next) {
      res.status(200).json(await queries.getAllResources())
  })
})

router.get('/:resourceId', async function(req, res, next) {
  // console.log("get resourceId: ", req.params.resourceId)
  return await handleRoute(req, res, next,
    async function(req, res, next) {
      resource = await queries.getResource(req.params.resourceId)
      if (resource) {
        res.status(200).json(resource)
      } else {
        res.sendStatus(404)
      }
  })
})

router.post('/', async function(req, res, next) {
  return await handleRoute(req, res, next,
    async function(req, res, next) {
      createResponse = commands.createResource(req.body)
      res.location(`/resources/${createResponse._id}`)
      res.sendStatus(201)
  })
})

router.put('/:resourceId', async function(req, res, next) {
  return await handleRoute(req, res, next,
    async function(req, res, next) {
      if (await commands.updateResource(req.params.resourceId, req.body)) {
        res.sendStatus(200)
      } else {
        res.sendStatus(404)
      }
  })
})

router.delete('/:resourceId', async function(req, res, next) {
  return await handleRoute(req, res, next,
    async function(req, res, next) {
      if (await commands.deleteResource(req.params.resourceId)) {
        res.sendStatus(200)
      } else {
        res.sendStatus(404)
      }
  })
})

module.exports = router;

const mongodb = require('mongodb')
const utils = require('./utils.js')
const eventResponse = require('./utils.js').eventResponse
const getDb = utils.getDb

var exports = module.exports = {}

eventResponse('resource-created',
  async function(resourceDb, data) {
    await resourceDb.collection(utils.resourceCollectionName)
      .insertOne(JSON.parse(data.value.toString()))
})

eventResponse('resource-deleted',
  async function(resourceDb, data) {
    incoming = JSON.parse(data.value.toString())
    filter = Object.assign({}, {"_id":incoming._id})
    await resourceDb.collection(utils.resourceCollectionName)
      .findOneAndDelete(filter)
})

eventResponse('resource-updated',
  async function(resourceDb, data) {
    incoming = JSON.parse(data.value.toString())
    filter = Object.assign({}, {"_id":incoming._id})
    await resourceDb.collection(utils.resourceCollectionName)
      .findOneAndUpdate(filter, {$set: incoming.resource})
})

exports.getAllResources = async function(resource) {
  try {
    var resources = []
    var resourceDb = await getDb()
    await resourceDb.collection(utils.resourceCollectionName).find({})
      .forEach( function(resource) { resources.push(resource) })
    return {"resources":resources}
  } catch (err) {
    console.error(err);
    throw new Error('get all resources failed', err)
  }
}

exports.getResource = async function(resourceId) {
  try {
    var resourceDb = await getDb()
    console.log("find by resourceId: ", resourceId)
    var filter = Object.assign({}, {"_id" : resourceId})
    var cursor = resourceDb.collection(utils.resourceCollectionName).find(filter)
    var count = await cursor.count()
    if (count > 0) {
      return await cursor.next()
    } else {
      return null
    }
  } catch (err) {
    console.error(err);
    throw new Error('get resource failed', err)
  }
}

var uuidv4 = require('uuid/v4')
const Kafka = require('node-rdkafka')
const getResource = require('./resource-queries.js').getResource

var exports = module.exports = {}

var producer = null;
var wait_on_producer = new Kafka.Producer({
  'metadata.broker.list': 'rskc-kafka-bootstrap.resource-scheduler.svc.cluster.local:9092',
})

// Wait for the ready event before proceeding
wait_on_producer.on('ready', function() {
  console.log('producer ready')
  producer = wait_on_producer;
});

// Any errors we encounter, including connection errors
wait_on_producer.on('error', function(err) {
  console.error('Error from producer');
  console.error(err);
})

// Connect to the broker manually
wait_on_producer.connect();

exports.ready = function() {
  return producer ? true : false
}

async function resourceExists(resourceId) {
  try {
    resource = await getResource(resourceId)
    return resource ? true : false
  } catch (err) {
    throw new Error("error checking if resource exists")
  }
}

async function executeIfExists(resourceId, operationParameters, handler) {
  try {
    if (await resourceExists(resourceId)) {
      return {"exists": true, "returnValue": await handler(operationParameters)}
    } else {
      return {"exists": false}
    }
  } catch(err) {
    throw new Error("error executing executeIfExists")
  }
}

exports.createResource = function(resource) {
  try {
    resource._id = uuidv4()
    producer.produce('resource-created', null,
      Buffer.from(JSON.stringify(resource)), null, Date.now())
    return resource
  } catch (err) {
    console.error(err);
    throw new Error('Save failed')
  }
  return
}

exports.deleteResource = async function(resourceId) {
  try {
    returnValue =  await executeIfExists(resourceId, {},
      async function(operationParameters) {
        producer.produce('resource-deleted', null,
          Buffer.from(JSON.stringify({"_id": resourceId})), null, Date.now())
    })
    return returnValue.exists
  } catch (err) {
    console.error(err);
    throw new Error('Delete failed')
  }
  return
}

exports.updateResource = async function(resourceId, resource) {
  try {
    returnValue =  await executeIfExists(resourceId, {"resource":resource },
      async function(operationParameters) {
        producer.produce('resource-updated', null,
          Buffer.from(JSON.stringify({"_id": resourceId,
          "resource": operationParameters.resource})), null, Date.now())
    })
    return returnValue.exists
  } catch (err) {
    console.error(err);
    throw new Error('Update failed')
  }
  return
}

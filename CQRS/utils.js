
const mongodbClient = require('mongodb').MongoClient
const Kafka = require('node-rdkafka')

var exports = module.exports = {}

exports.resourceCollectionName = 'resources'
var db = null;

function connectToMongo(mongoURL) {
  return new Promise(
    (resolve, reject) => {
      mongodbClient.connect(mongoURL, function(err, client) {
          if (err) { reject(err) }
          resolve(client)
        })
   })
}

const getDb = async function() {
  var mongoHost, mongoPort, mongoDatabase, mongoPassword, mongoUser;
  var client = null

  if (!db) {
    mongoDatabase = encodeURIComponent(process.env.mongoDatabase)
    mongoPassword = encodeURIComponent(process.env.mongoPassword)
    mongoUser = encodeURIComponent(process.env.mongoUser)
    mongoHost = encodeURIComponent(process.env.mongoHost)
    mongoPort = encodeURIComponent(process.env.mongoPort)
    mongoAuthMechanism = encodeURIComponent(process.env.mongoAuthMechanism)
    // console.log("database env variables: ", mongoHost, mongoDatabase, mongoPort, mongoUser, mongoPassword)

    mongoURL = `mongodb://${mongoUser}:${mongoPassword}@${mongoHost}:${mongoPort}/?authMechanism=${mongoAuthMechanism}&authSource=${mongoDatabase}`
    client = await connectToMongo(mongoURL)
    if (!client) {
      throw new Error('database connection error')
    }

    db = client.db(mongoDatabase)
  }
  return db
}

exports.getDb = getDb

function createConsumer() {
  return new Kafka.KafkaConsumer({
    'group.id': 'kafka',
    'metadata.broker.list': 'rskc-kafka-bootstrap.resource-scheduler.svc.cluster.local:9092',
  }, {})
}

exports.eventResponse = async function(event, responseFunction) {
  var consumer = createConsumer()
  consumer
    .on('ready', function() {
      consumer.subscribe([event]);
      consumer.consume();
    })
    .on('data', async function(data) {
      var resourceDb = await getDb()
      response = await responseFunction(resourceDb, data)
    })
  consumer.connect();
}
